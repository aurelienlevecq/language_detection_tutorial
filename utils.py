import pandas as pd
from sklearn.model_selection import StratifiedShuffleSplit
import numpy as np

def read_csv_data(csv_path: str) -> pd.DataFrame:
    """
    Read a csv file with pandas and return its data frame
    """
    data_frame = pd.read_csv(csv_path, sep=",")
    return data_frame


def prepare_data_sets(data_frame) -> list:
    """
    Split the data set into train,dev and test
    """
    """Split the data frame into train,dev and test set
    and store the 3 files"""

    sss = StratifiedShuffleSplit(n_splits=1, test_size=0.1, random_state=42)
    for train_index, test_index in sss.split(np.asarray(data_frame["Text"]),
                                             np.asarray(data_frame["language"])):
        x_train, x_test = np.asarray(data_frame["Text"])[train_index], \
                              np.asarray(data_frame["Text"])[test_index]
        y_train, y_test = np.asarray(data_frame["language"])[train_index], \
                              np.asarray(data_frame["language"])[test_index]

    

    dataframe_train = pd.DataFrame({"text": x_train, "label": y_train})


    dataframe_test = pd.DataFrame({"text": x_test, "label": y_test})

    return dataframe_train,  dataframe_test



def evaluate():
    pass


def tokenize_text(text):
    tokenized_text = text.split(" ")
    return tokenized_text
