import os
import pickle

from tensorflow.python.keras.callbacks import EarlyStopping, ModelCheckpoint
from sklearn.metrics import classification_report
from sentence_model import SentenceModel
from utils import read_csv_data, prepare_data_sets, tokenize_text
from tensorflow.keras.preprocessing.sequence import pad_sequences
import numpy as np
import tensorflow as tf

# Pipeline pour creer le model baser sur les phrases

df = read_csv_data("dataset.csv")

train, test = prepare_data_sets(df)
print("nombre d'entrées dans train ", len(train))
print("nombre d'entrées dans test ", len(test))
train_text = train["text"].tolist()
train_label = train["label"].tolist()

test_text = test["text"].tolist()
test_label = test["label"].tolist()

train_text_tokenized = []
for i in train_text:
    tokenized_text = tokenize_text(text=i)
    train_text_tokenized.append(tokenized_text)

test_text_tokenized = []
for i in test_text:
    tokenized_text = tokenize_text(text=i)
    test_text_tokenized.append(tokenized_text)

list_words = []
for text in train_text_tokenized:
    for word in text:
        list_words.append(word)

set_words = set(list_words)
word_2_id = {"PAD": 0, "UKN": 1}
for word in set_words:
    word_2_id[word] = len(word_2_id)


def convert_word_2_id(sentence, word_to_id):
    sentence_2_id = []
    for wrd in sentence:
        sentence_2_id.append(word_to_id.get(wrd, word_2_id["UKN"]))
    return sentence_2_id


train_text_id = [convert_word_2_id(sentence, word_2_id) for sentence in train_text_tokenized]
train_text_id = pad_sequences(train_text_id, maxlen=40, padding="post")

test_text_id = [convert_word_2_id(sentence, word_2_id) for sentence in test_text_tokenized]
test_text_id = pad_sequences(test_text_id, maxlen=40, padding="post")

label_2_id = {}
for label in set(train_label):
    label_2_id[label] = len(label_2_id)
y_train = [label_2_id[label] for label in train_label]
y_test = [label_2_id[label] for label in test_label]
y_train = np.asarray(y_train)
sentence_model = SentenceModel()

# Utilities in order to save the best trained model
output_model_filepath = "language_nn.h5"
mcp_save = ModelCheckpoint(output_model_filepath, save_best_only=True, monitor='val_loss', mode='min')

model = sentence_model.create_model_lstm(vocabulary_size=len(word_2_id), embedding_dim=50, num_class=len(label_2_id))
model.compile(optimizer='adam', loss="sparse_categorical_crossentropy",
              # metrics=['accuracy', tf.keras.metrics.Precision(), tf.keras.metrics.Recall()]
              )
epochs = 30
history = model.fit(
    train_text_id, y_train,
    validation_split=0.3,
    epochs=epochs,
    batch_size=1024,
    callbacks=[mcp_save]
)
id_2_label = {i: label for label, i in label_2_id.items()}

with open("word_2_id.pck", "wb") as o:
    pickle.dump(word_2_id, o)
with open("id_2_label.pck", "wb") as o:
    pickle.dump(id_2_label, o)

test_prediction_result = model.predict(test_text_id, verbose=1)
predicted_languages = []
for result in test_prediction_result:
    pos_language = np.argmax(result)
    predicted_languages.append(id_2_label[pos_language])

y_test = [id_2_label[language] for language in y_test]  # put language instead of id for be human friendly
print(classification_report(y_true=y_test, y_pred=predicted_languages))
# préparer les fonctions pour traiter l'entrée utilisateur
