import pickle

import numpy as np
import tensorflow as tf
from tensorflow.python.keras.preprocessing.sequence import pad_sequences


class SentenceModel:
    def predict_language(self, text: str):
        model = tf.keras.models.load_model("language_nn.h5")
        with open("word_2_id.pck","rb") as f:
            word_2_id = pickle.load(f)
        with open("id_2_label.pck","rb") as f:
            id_2_label = pickle.load(f)
        text = text.split(" ")
        text_word_id = []
        for word in text:
            text_word_id.append(word_2_id.get(word,word_2_id["UKN"]))
        sentences = pad_sequences([text_word_id], maxlen=40, padding="post")
        prediction = model.predict(sentences)
        prediction = prediction[0]
        pos_language = np.argmax(prediction)
        return id_2_label[pos_language]

    def create_model_dense(self, vocabulary_size, embedding_dim, num_class):
        model = tf.keras.Sequential([
            tf.keras.layers.Embedding(vocabulary_size, embedding_dim),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.GlobalAveragePooling1D(),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Dense(num_class, activation="softmax")])

        model.summary()
        return model

    def create_model_lstm(self, vocabulary_size, embedding_dim, num_class):
        model = tf.keras.Sequential([
            tf.keras.layers.Embedding(vocabulary_size, embedding_dim),
            tf.keras.layers.Dropout(0.2),
            tf.keras.layers.Bidirectional(tf.keras.layers.LSTM(64)),
            tf.keras.layers.Dense(64, activation='relu'),
            tf.keras.layers.Dense(num_class, activation="softmax")])

        model.summary()
        return model


